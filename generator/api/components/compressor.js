// import plugins
import archiver from "archiver";
import { OUTPUT_URL } from '../../helpers/settings';
import fs from "fs";

// compressor
export const compressor = name =>
	new Promise((resolve, reject) => {
		// settings
		let output = fs.createWriteStream(OUTPUT_URL + `/${name}.zip`);
		let archive = archiver("zip", {
			zlib: { level: 9 }
		});

		// pipe
		archive.pipe(output);
		// archive
		archive.directory(`${OUTPUT_URL+name}`, false);
		// finalize
		archive
			.finalize()
			.then(res => resolve(name))
			.catch(err => reject(err));
	});
