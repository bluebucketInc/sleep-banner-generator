
// lib imports
import ejs from 'ejs';
import fs from 'fs-extra';
import path from 'path';
import { OUTPUT_URL } from '../../helpers/settings';

// modules
import { getConfig, sanitizeFilename, getRandID } from '../../helpers';

// variables
const templatePath = path.join(__dirname, '..', '..', 'templates/');

// generate
export const generator = data =>
	new Promise(async (resolve, reject) => {
		// params
		const jobName = data.jobName;
		const templateID = data.templateID;
		const jobData = data.jobData;
	
		// config
		const config = await getConfig(templateID).then(res => res.default);
		const jobDir = `${sanitizeFilename(jobName)}_${getRandID(5)}`;
		const outputDir = path.join(OUTPUT_URL, jobDir, '/');
		const genContent = jobData;

		// create current job folder
		await fs.ensureDir(outputDir);
	
		// create job loops
		const promiseChain = [];
		// add loop
		jobData.map((data, index) =>
			promiseChain.push(
				variantGenerator(config, `${outputDir+(index+1)}.${sanitizeFilename(data.bannerName)}_${getRandID(2)}/`, data)
			)
		);

		// resolve promise
		Promise.all(promiseChain)
			.then(res => resolve(jobDir))
			.catch(err => reject(err));
		
	});



// variant generator
const variantGenerator = (config, outputPath, genContent) =>
	new Promise((resolve, reject) => {
		const promiseChain = [];
		// create promise loop
		for (let i = 0; i < config.variants.length; i++) {
			// vars
			let _outputPath = outputPath + config.variants[i].name + '/';
			let _imgSrc = `${templatePath + config.templateID}/content/${config.variants[i].name}/`;
			let _templateSrc = `${templatePath + config.templateID}/content/${config.variants[i].name}/index.ejs`;
			// copy image files
			config.variants[i].img.map(img => fs.copySync(_imgSrc + img, _outputPath + img));
			// push to promise array
		    promiseChain
		    	.push(new Promise((resolve,reject) =>
					ejs.renderFile(_templateSrc, genContent, (err, htmlData) =>
						fs.outputFile(_outputPath + 'index.html', htmlData, 'utf8')
							.then(() => resolve('file created'))
							.catch(err => reject(err))
					)
			    ));
		}
		// resolve promise
		Promise.all(promiseChain)
			.then(res => resolve())
			.catch(err => reject(err));
	});





