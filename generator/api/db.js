
// import plugins
import express from "express";
import mongoose from "mongoose";
import { DB_URL, DB_USER, DB_PASS } from '../helpers/settings';

// router
export const dbRoutes = express.Router();

// DATABASE CONNECTION //

// schema
const banner_history = require('../db/db.schema');
// Connect to db
mongoose.connect(DB_URL, { useNewUrlParser: true, user: DB_USER, pass: DB_PASS });
// set connection
const connection = mongoose.connection;
// callback

// get all
dbRoutes.route('/get').get(async (req, res) => {
	// req
	let result = await banner_history.find({});
	// send
	res.status(200).send({ data: result });
});

// delete
dbRoutes.route('/delete').get(async (req, res) =>
	banner_history.deleteMany({}, () =>
		res.status(200).send({data: 'all data gone'})
	));

// save to db
export const SaveItemtoDB = (payload, output) =>
	new Promise((resolve, reject) => {
		// db item
	    let item = new banner_history({
		    date: Date.now(),
		    job_name: payload.jobName,
		    folder: output,
		    template_id: payload.templateID,
		});
	    // send
	    item.save()
	        .then(todo => resolve(output))
	        .catch(err => reject(err));
	});