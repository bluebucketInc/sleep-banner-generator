// import plugins
import express from "express";
import { join } from "path";

// components
import { generator } from './components/generator';
import { compressor } from './components/compressor';

// settings
import { API_PORT } from '../helpers/settings';
import { getOutputURls } from "../helpers";

// db
import { SaveItemtoDB, ClearAllinDB } from './db';

// router
const genRoutes = express.Router();

// generator endpoint
genRoutes.post("/generator", (req, res) => {

	// call generator
	generator(req.body)
		.then(folder => compressor(folder))
		.then(name => SaveItemtoDB(req.body, name))
		.then(name =>
			res.status(201).send({
				success: true,
				...getOutputURls(name)
			})
		)
		.catch(err => {
			console.log(err);
			res.status(400).send({
				success: false,
				message: err
			});
		});
});

// export
module.exports = genRoutes;