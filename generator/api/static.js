
// import plugins
import express from "express";
import { join } from "path";
import { OUTPUT_URL } from '../helpers/settings';
import serveIndex from "serve-index";

// router
const staticRoutes = express.Router();

// serve static files for output
staticRoutes.use(
	"/output",
	express.static(OUTPUT_URL),
	serveIndex(OUTPUT_URL, { icons: true })
);

// static for website
staticRoutes.use(
	"/static",
	express.static(join(__dirname, '..', "ui", "build", "static"))
);

// public folder
staticRoutes.use(
	"/public",
	express.static(join(__dirname, '..', "public"))
);

// UI Page
staticRoutes.get("/", function(req, res) {
	res.sendFile(join(__dirname, '..', "ui", "build", "index.html"));
});

// export
module.exports = staticRoutes;