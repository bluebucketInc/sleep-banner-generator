// lib imports
import mongoose from "mongoose";
const Schema = mongoose.Schema;

// DB NAME
// banner_history

// schema
let banner_history = new Schema({
    date: Date,
    job_name: String,
    template_id: String,
    folder: String,
});

// export
module.exports = mongoose.model("banner_history", banner_history);
