

// lib imports
import mkdirp from "mkdirp";
import { API_PORT, PROD_DOMAIN, DEV_DOMAIN } from './settings';

// get template config
export const getConfig = tempID => import(`../templates/${tempID}/config.js`);

// sanitize filename
export const sanitizeFilename = filename =>
	filename.replace(/[^a-z0-9]/gi, "_").toLowerCase();

// generate rand id
export const getRandID = strength =>
	Math.random()
		.toString(36)
		.substr(2, strength);

// get preview & download urls
export const getOutputURls = name => {
	return {
		preview: (process.env.NODE_ENV === "prod" ? PROD_DOMAIN : DEV_DOMAIN ) + name,
		download: (process.env.NODE_ENV === "prod" ? PROD_DOMAIN : DEV_DOMAIN ) + name + '.zip'
	};
};