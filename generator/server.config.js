
// PM2 server config
module.exports = {
    apps: [
        {
            name: "banner-generator",
            script: "./server.js",
            instances: 0,
            exec_mode: "cluster",
            interpreter: "./node_modules/.bin/babel-node",
            ignore_watch: ["node_modules"],
            watch: true,
            env: {
                NODE_ENV: "production",
                PORT: "4000"
            }
        }
    ]
};
