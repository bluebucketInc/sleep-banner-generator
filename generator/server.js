// import plugins
import express from "express";
import bodyParser from "body-parser";
import cors from 'cors';
import helmet from 'helmet';

// settings
import { API_PORT } from "./helpers/settings";

// API routes
import genRoutes from "./api/generator";
import staticRoutes from './api/static';
import { dbRoutes } from './api/db';

// init server
const server = express();

// add middleware
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

// security
server.use(helmet());

// CORS
server.use(cors());

// static & api & db
server.use("/api", genRoutes);
server.use("/db", dbRoutes);
server.use("/", staticRoutes);

// broadcast
server.listen(API_PORT, () => console.log(`server running on port ${API_PORT}`));