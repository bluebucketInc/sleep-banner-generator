/*
    template config
    a = 300x250
    b = 300x600
    c = 728x90
    d = 970x250
*/

// template config
const config = {
    templateID: "AIA-SLP-01",
    textA: "Sunshine helps you sleep better in the nighttime",
    textB: "Getting enough sunlight can help your body clock stay in tune with the rhythm of the day. One tip to help you get",
    cta: "https://www.google.com",
    variants: [
        {
            name: "300x250",
            template: "index.ejs",
            img: ["logo.png"],
        },
        {
            name: "300x600",
            template: "index.ejs",
            img: ["logo.png"],
        },
        {
            name: "728x90",
            template: "index.ejs",
            img: ["logo.png"],
        },
        {
            name: "970x250",
            template: "index.ejs",
            img: ["logo.png"],
        },
    ]
};

// export
export default config;
