// lib imports
import React from "react";
import axios from "axios";

// material
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';

// components
import FileParser from "./_fileParser";
import ErrorSnack from './_errorSnack';
import OutputCard from './_outputCard';
import HistoryView from './_history';
import Form from './_form';

// utils
import { DOMAIN, ENDPOINTS } from "../utils";
import { formStyles } from "../styles/theme";
import { initialState, reducer } from "./_app_reducer";

// Our app
const App = () => {
	// hooks
	const classes = formStyles();
	const [state, dispatch] = React.useReducer(reducer, initialState);

	// handle submit
	const handleSubmit = () => {

		// handle error
		if(!state.genData.isFileLoaded || !state.meta.jobName || !state.meta.templateID) {
			handleFormError(true);
			return;
		} else {
			// clear error
			handleFormError(false);
		}

		// set processing
		dispatch({ type: "set-processing", payload: true});

		// request
		axios
			.post(DOMAIN + ENDPOINTS.gen, {
				jobData: state.genData.data,
				jobName: state.meta.jobName,
				templateID: state.meta.templateID,
			})
			.then(res => {
				// stop processing
				dispatch({ type: "set-processing", payload: false});
				// set response data
				dispatch({ type: "set-response", payload: res.data });
			})
			.catch(err => {
				// stop processing
				dispatch({ type: "set-processing", payload: false});
				// set error
				dispatch({ type: "output-error"});
			});
	};

	// get & set history
	const getHistory = () => axios.get(DOMAIN + ENDPOINTS.db_get).then(res => dispatch({ type: "set-history", payload: res.data.data }));

	// handle fileupload
	const handleFileUpload = data =>
		dispatch({
			type: data ? "genData-add" : "genData-remove",
			payload: data
		});

	// handle reset
	const handleReset = data =>
		dispatch({ type: "hard-reset" });

	// form error
	const handleFormError = state => dispatch({ type: "genData-error", payload: state});

	// render
	return (
		<React.Fragment>
			<div className="outer-wrapper">
				<Paper className="app-wrapper">
					{/* normalizer */}
					<CssBaseline />
					{/* Error Popups */}
					<ErrorSnack open={state.genData.isError} setOpen={handleFormError} message={"Please fill the form completely"} />
					<ErrorSnack open={state.output.isError} setOpen={handleFormError} message={"Oops something went wrong!"} />
					{/* upload form */}
					<Form {...state.meta} isProcessing={state.genData.isProcessing} dispatch={dispatch} />
					<FileParser
						onRemoveFile={handleFileUpload}
						onFileUpload={handleFileUpload}
						disabled={state.genData.isProcessing}
					/>
					{/* action buttons */}
					<div className={classes.submitWrapper}>
						<Button
							variant="contained"
							className={classes.button}
							disabled={state.genData.isProcessing}
							onClick={handleSubmit}
							color="primary"
						>
							Generate
						</Button>
						{state.genData.isProcessing && (
							<CircularProgress
								size={24}
								className={classes.buttonProgress}
							/>
						)}
					</div>
				</Paper>
				{/* output */}
				<OutputCard output={state.output} handleReset={handleReset} />
				{/* history */}
				<HistoryView classes={classes} data={state.history} getHistory={getHistory} />
			</div>
		</React.Fragment>
	);
};

// export
export default App;
