
// imports
import { HISTORY_LIMIT } from '../utils';

// initial state
export const initialState = {
	meta: {
		jobName: "",
		templateID: "",
	},
	genData: {
		data: [],
		isFileLoaded: false,
		isProcessing: false,
		isError: false,
	},
	output: {
		isReady: false,
		isError: false,
		previewLink: "",
		downloadLink: "",
	},
	history: [],
};

// reducer
export const reducer = (state, action) => {
	switch (action.type) {
		case "meta":
			return {
				...state,
				meta: {
					...state.meta,
					...action.payload
				}
			};
		case "genData-add":
			return {
				...state,
				genData: {
					...state.genData,
					data: action.payload,
					isFileLoaded: true
				}
			};
		case "genData-remove":
			return {
				...state,
				genData: {
					...state.genData,
					data: [],
					isFileLoaded: false
				}
			};
		case "genData-error":
			return {
				...state,
				genData: {
					...state.genData,
					isError: action.payload,
				}
			};
		case "set-processing":
			return {
				...state,
				genData: {
					...state.genData,
					isProcessing: action.payload,
				}
			};
		case "output-error":
			return {
				...state,
				output: {
					...state.output,
					isError: true,
				}
			};
		case "set-response":
			return {
				...state,
				output: {
					...state.output,
					isReady: true,
					isError: false,
					previewLink: action.payload.preview,
					downloadLink: action.payload.download,
				}
			};
		case "set-history":
			return {
				...state,
				history: action.payload.reverse().slice(0, HISTORY_LIMIT),
			};
		case "hard-reset":
			return {
				...initialState,
				
			};
		default:
			throw new Error();
	}
};