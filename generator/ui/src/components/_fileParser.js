// lib imports
import React from "react";
import Papa from "papaparse";
import { FilePond, registerPlugin } from "react-filepond";
import JsonTable from "ts-react-json-table";

// material design
import Button from '@material-ui/core/Button';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Link from '@material-ui/core/Link';
// filepond settings
import "filepond/dist/filepond.min.css";
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
registerPlugin(FilePondPluginFileValidateType);

// file parser
const FileParser = ({ onFileUpload, onRemoveFile, disabled }) => {

    // hook
    const [state, setState] = React.useState({ data: "" });
    const [open, setOpen] = React.useState(false);

    // pond instance
    let pond = {};
    // on add file
    const onFileUpdate = files => {
        const file = pond.getFile().file;
        if (file) {
            Papa.parse(file, {
                header: true,
                complete: res => {
                    setState({ data: res.data });
                    return onFileUpload(res.data);
                }
            });
        }
    };

    // render
    return (
        <div className="upload-container">

            {/* file parser */}
            <FilePond
                ref={ref => (pond = ref)}
                onaddfile={onFileUpdate}
                onremovefile={() => {
                    onRemoveFile();
                    setState({ data: "" });
                }}
                allowMultiple={false}
                acceptedFileTypes={["application/vnd.ms-excel", "text/csv", "application/csv"]}
                labelIdle={
                    'Drag & drop the CSV file or <span class="filepond--label-action"> Browse </span>'
                }
                disabled={disabled}
            />

            {/* action buttons */}
            <p>Download a <Link href="/public/csv/csv-template.csv">Sample CSV</Link> template to see an example of the format required.</p>
            <Button disabled={state.data ? false : true} variant="outlined" color="default" onClick={() => setOpen(true)}>Preview CSV</Button>

            {/* Preview popup */}
            <Dialog
                maxWidth={"lg"}
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="max-width-dialog-title"
            >
                <DialogTitle id="max-width-dialog-title">CSV Preview</DialogTitle>
                <DialogContent>
                    <DialogContentText>If this preview doesn’t look right, please re-upload the CSV. You will be generating approximately <strong className="banner-item-number">{state.data.length || "X"}</strong> banners with this CSV.</DialogContentText>
                    {state.data && (
                        <JsonTable theadClassName="table-head" rows={state.data} />
                    )}
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpen(false)} color="primary">Close</Button>
                </DialogActions>
            </Dialog>

        </div>
    );
};

// export
export default FileParser;
