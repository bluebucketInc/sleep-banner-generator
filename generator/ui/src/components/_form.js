// lib imports
import React from "react";

// material
import TextField from "@material-ui/core/TextField";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';

// components
import { formStyles } from "../styles/theme";

// form component
const Form = ({ jobName, templateID, isProcessing, dispatch }) => {
	// get style
	const classes = formStyles();

	// render
	return (
		<>
			<Typography variant="h6" gutterBottom>
				Banner Details
			</Typography>
			<form className={classes.container} noValidate autoComplete="off">
				<TextField
					required
					id="job-name"
					label="Job Name"
					className={classes.textField}
					value={jobName}
					onChange={e =>
						dispatch({
							type: "meta",
							payload: { jobName: e.target.value }
						})
					}
					margin="normal"
					InputProps={{ readOnly: isProcessing }}
				/>
				<FormControl className={classes.formControl}>
					<InputLabel required htmlFor="job-id">
						Template ID
					</InputLabel>
					<Select
						value={templateID}
						onChange={e =>
							dispatch({
								type: "meta",
								payload: { templateID: e.target.value }
							})
						}
						inputProps={{
							name: "age",
							id: "job-id",
							readOnly: isProcessing
						}}
					>
						<MenuItem value={"AIA-SLP-01"}>AIA-SLP-01</MenuItem>
					</Select>
				</FormControl>
			</form>
		</>
	);
};

// export
export default Form;