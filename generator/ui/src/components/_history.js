// lib imports
import React from "react";
import moment from 'moment';
import ErrorIcon from "@material-ui/icons/History";
import JsonTable from "ts-react-json-table";

// material
import Button from '@material-ui/core/Button';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

// domain
import { DOMAIN } from "../utils";

// Our app
const HistoryView = ({ data, classes, getHistory }) => {

	// hook
	const [open, setOpen] = React.useState(false);

	// table columns
	var columns = [
		{ key: 'Date', cell: (row, columnKey) => moment(row.date).format('DD/MM/YYYY')},
		{ key: 'job_name', label: 'Job Name'},
		{ key: 'template_id', label: 'Template'},
		{ key: 'folder', label: ' ', cell: (row, columnKey ) =>
			<React.Fragment>
				<a href={`${DOMAIN}output/${row.folder}`} target="_blank" rel="noopener noreferrer">
					<Button
						className={classes.button}
						color="primary"
						variant="outlined"
					>Preview</Button>
				</a>
				<a href={`${DOMAIN}output/${row.folder}.zip`} target="_blank" rel="noopener noreferrer">
					<Button
						className={classes.button}
						color="secondary"
						variant="outlined"
					>Download</Button>
				</a>
			</React.Fragment>
		},
	];

	// render
	return (
		<>
			<div className="hisory-container" onClick={() => { setOpen(true); getHistory(); }}>
				<ErrorIcon className={classes.icon} />
			</div>
	        <Dialog
	            maxWidth={"lg"}
	            open={open}
	            onClose={() => setOpen(false)}
	            aria-labelledby="max-width-dialog-title"
	        >
	            <DialogTitle id="max-width-dialog-title">Generator history</DialogTitle>
	            <DialogContent>
	            	Latest 10 jobs
	                <JsonTable theadClassName="table-head" rows={data} columns={columns} />
	            </DialogContent>
	            <DialogActions>
	                <Button onClick={() => setOpen(false)} color="primary">Close</Button>
	            </DialogActions>
	        </Dialog>
	    </>
	);
};

// export
export default HistoryView;