
// lib imports
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Collapse from '@material-ui/core/Collapse';

// styles
const useStyles = makeStyles({
    card: {
        maxWidth: 600,
        textAlign: "center"
    },
    buttonGroup: {
        justifyContent: "flex-end"
    }
});

// component
const OutputCard = ({ output, handleReset }) => {

    // classes
    const classes = useStyles();
    // render
    return (
        <Collapse in={output.isReady}>
            <Card className={classes.card}>
                <CardActionArea>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h4">Banner generated successfully!</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            You can preview or download the generated banners here.
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions className={classes.buttonGroup}>
                    <Button size="small" color="primary" onClick={handleReset}>
                        Reset
                    </Button>
                    <a href={output.previewLink} target="_blank" rel="noopener noreferrer">
                        <Button size="small" color="secondary">
                            Preview
                        </Button>
                    </a>
                    <a href={output.downloadLink} target="_blank" rel="noopener noreferrer">
                        <Button size="small" color="secondary">
                            Download
                        </Button>
                    </a>
                </CardActions>
            </Card>
        </Collapse>
    );
}

// export
export default OutputCard;
