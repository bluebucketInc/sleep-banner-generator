
// lib imports
import React from 'react';
import ReactDOM from 'react-dom';
import * as Sentry from '@sentry/browser';

// styles
import "./styles/_app.scss";
import "./styles/_table.scss";

// components
import App from './components/_app';

// error tracking
Sentry.init({dsn: "https://762ccf09ef9f4c22901a5b6905264ad8@sentry.io/1473342"});

// dom render
ReactDOM.render(<App />, document.getElementById('root'));