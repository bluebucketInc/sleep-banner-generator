
// m.design
import { makeStyles } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";


// custom styles
export const formStyles = makeStyles(theme => ({
	container: {
		display: "flex",
		flexWrap: "wrap",
		flexDirection: "column",
	},
	submitWrapper: {
		margin: theme.spacing(1),
		position: "relative",
		alignSelf: "flex-end",
		marginRight: 70,
		marginTop: 30,
	},
	textField: {
		width: 450
	},
	button: {
		margin: theme.spacing(1)
	},
	buttonProgress: {
		color: green[500],
		position: "absolute",
		top: "50%",
		left: "50%",
		marginTop: -12,
		marginLeft: -12
	},
	formControl: {
		marginTop: theme.spacing(5),
		marginBottom: theme.spacing(5)
	},
	icon: {
        fontSize: 30,
        opacity: 1
    },
}));