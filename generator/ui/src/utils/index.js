
// settings
export const
	API_PORT = 4000,
	DOMAIN = window.location.href,
	HISTORY_LIMIT = 10,
	ENDPOINTS = {
		gen: 'api/generator',
		db_get: 'db/get',
		db_set: 'db/set'
	};